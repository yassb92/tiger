/**
 ** \file misc/singleton.hh
 ** \brief Generic singleton
 */

#pragma once

namespace misc
{
  template <typename T> class Singleton
  {
    // DONE
    public:
      static T* instance()
      {
        static T* instance;
        return instance;
      }
    protected:
      Singleton() {}
      ~Singleton() {}
    
    public:
      Singleton(const Singleton&) = delete;
      Singleton(Singleton&&) = delete;
      Singleton& operator=(Singleton&&) = delete;
      Singleton& operator=(const Singleton&) = delete; 
  };

} // namespace misc

