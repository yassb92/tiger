/** \file misc/scoped-map.hxx
 ** \brief Implementation of misc::scoped_map.
 */

#pragma once

#include <map>
#include <sstream>
#include <stdexcept>
#include <type_traits>

#include <ranges>
#include <vector>
#include <stack>
#include <misc/algorithm.hh>
#include <misc/contract.hh>
#include <misc/indent.hh>
#include "scoped-map.hh"

namespace misc
{
  // DONE: Some code was deleted here.

  template <typename Key, typename Data> scoped_map<Key, Data>::scoped_map()
  {
   // this->scoped_map_ = std::vector<std::map<Key, Data>>(1);
    this->scoped_map_ = std::stack<std::map<Key, Data>>();
  }

  template <typename Key, typename Data>
  void scoped_map<Key, Data>::scope_begin()
  {
    if (scoped_map_.size() == 0)
      scoped_map_.push(std::map<Key, Data>());
    else
      {
        std::map<Key, Data> new_scope = std::map<Key, Data>();
        std::map<Key, Data> scope = this->scoped_map_.top();
        for (typename std::map<Key, Data>::iterator it = scope.begin(); it != scope.end(); it++)
          {
            new_scope.insert({it->first, it->second});
          }
        scoped_map_.push(new_scope);
      };
  }

  template <typename Key, typename Data> void scoped_map<Key, Data>::scope_end()
  {
    scoped_map_.pop();
  }
  template <typename Key, typename Data>
  void scoped_map<Key, Data>::put(const Key& key, const Data& value)
  {
    if (this->scoped_map_.size() == 0)
      this->scoped_map_.push(std::map<Key, Data>());
    this->scoped_map_.top().insert_or_assign(key, value);
  }

  template <typename Key, typename Data>
  Data scoped_map<Key, Data>::get(const Key& key) const
  {
    if (this->scoped_map_.size() == 0)
    {
      if constexpr (std::is_pointer<Data>())
        return nullptr;
      throw std::range_error("Erreur\n");
    }
    std::map<Key, Data> scope = this->scoped_map_.top();
    if (scope.find(key) != scope.end())
      return scope.find(key)->second;
    else
      {
        if constexpr (std::is_pointer<Data>())
            return nullptr;
        throw std::range_error("Erreur\n");
      };
  }

  template <typename Key, typename Data>
  std::ostream& scoped_map<Key, Data>::dump(std::ostream& ostr) const
  {
    return ostr;
  }
  template <typename Key, typename Data>
  inline std::ostream& operator<<(std::ostream& ostr,
                                  const scoped_map<Key, Data>& tbl)
  {
    return tbl.dump(ostr);
  }

} // namespace misc
