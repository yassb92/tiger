/**
 ** \file type/named.cc
 ** \brief Implementation for type/named.hh.
 */

#include <set>

#include <type/named.hh>
#include <type/visitor.hh>

namespace type
{
  Named::Named(misc::symbol name)
    : name_(name)
    , type_(nullptr)
  {}

  Named::Named(misc::symbol name, const Type* type)
    : name_(name)
    , type_(type)
  {}

  // Inherited functions
  void Named::accept(ConstVisitor& v) const
  {
    // DONE
    v(*this);
  }

  void Named::accept(Visitor& v)
  {
    // DONE
    v(*this);
  }

  bool Named::sound() const
  {
    // DONE (Sound).
    if (this->actual() == *this)
    {
      return false;
    }
    return true;
  }

  bool Named::compatible_with(const Type& other) const
  {
    // DONE (Special implementation of "compatible_with" for Named).
    type_get()->compatible_with(other);
  }

} // namespace type
