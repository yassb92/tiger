/**
 ** \file type/array.hh
 ** \brief The class Array.
 */
#pragma once

#include <type/fwd.hh>
#include <type/type.hh>

namespace type
{
  /// Array types.
  class Array : public Type
  {
    // FIXME: Some code was deleted here.

  ///Construct an array type with elements of type \a type.
  Array(const Type& type);
  
  ///Accept a const visitor \a v.
  void accept(ConstVisitor& v) const override;
  ///Accept a non-const visitor \a v.
  void accept(Visitor& v) override;

  ///Return the type of the elements.
  const Type& element_type_get() const;
  ///Set the type of the elements.
  void element_type_set(const Type& type);


    private:
      /// The type of the elements.
      const Type& element_type_;


  };

} // namespace type

#include <type/array.hxx>
