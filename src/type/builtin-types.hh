/**
 ** \file type/builtin-types.hh
 ** \brief The classes Int, String, Void.
 */
#pragma once

#include <misc/singleton.hh>
#include <type/fwd.hh>
#include <type/type.hh>

namespace type
{
  class Int : public misc::Singleton<Int>, public Type
  {

    private:
      Int() {}
      ~Int() {}
  };

  class String : public misc::Singleton<String>, public Type
  {
    private:
      String() {}
      ~String() {}
  };

  class Void : public misc::Singleton<Void>, public Type
  {
    private:
      Void() {}
      ~Void() {}
  };
} // namespace type
