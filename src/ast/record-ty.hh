/**
 ** \file ast/record-ty.hh
 ** \brief Declaration of ast::RecordTy.
 */

#pragma once

#include <ast/field.hh>
#include <ast/ty.hh>

namespace ast
{
  /// RecordTy.
  class RecordTy : public Ty
  {
  public:
    // DONE
    RecordTy(const Location& location, ast::fields_type* fields);
    RecordTy(const RecordTy&) = delete;
    RecordTy& operator=(const RecordTy&) = delete;
    /// Destroy a RecordTy node.
    /** \} */

    /// \name Visitors entry point.
    /// \{ */
    /// Accept a const visitor \a v.
    void accept(ConstVisitor& v) const override;
    /// Accept a non-const visitor \a v.
    void accept(Visitor& v) override;
    /// \}

    /** \name Accessors.
     ** \{ */
    /// Return the name of the type.
    ast::fields_type* fields_get() const;
    /// Set the name of the type.
    void fields_set(ast::fields_type*);
    /// Return definition site.
    const TypeDec* def_get() const;
    /// Return definition site.
    TypeDec* def_get();
    /// Set definition site.
    void def_set(TypeDec*);
    /** \} */

  protected:
    /// The name of the type.
    ast::fields_type* fields_;
    /// Definition site.
    TypeDec* def_ = nullptr;
  };
} // namespace ast
#include <ast/record-ty.hxx>
