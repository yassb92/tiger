/**
 ** \file ast/let-exp.hh
 ** \brief Declaration of ast::LetExp.
 */

#pragma once

#include <ast/chunk-list.hh>
#include <ast/exp.hh>
#include <misc/contract.hh>

namespace ast
{
  /// LetExp.
  class LetExp : public Exp
  {
  public:
    // DONE
    LetExp(const Location& location, ast::ChunkList* decs, ast::Exp* body);
    LetExp(const LetExp&) = delete;
    LetExp& operator=(const LetExp&) = delete;
    /// Destroy an LetExp node.
    /** \} */

    /// \name Visitors entry point.
    /// \{ */
    /// Accept a const visitor \a v.
    void accept(ConstVisitor& v) const override;
    /// Accept a non-const visitor \a v.
    void accept(Visitor& v) override;
    /// \}

    /** \name Accessors.
     ** \{ */
    /// Return stored integer value.
    ast::ChunkList* decs_get() const;
    ast::Exp* body_get() const;
    /** \} */

  protected:
    /// Stored integer value.
    ast::ChunkList* decs_;
    ast::Exp* body_;
  };
} // namespace ast
#include <ast/let-exp.hxx>
