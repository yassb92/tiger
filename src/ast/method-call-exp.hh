/**
 ** \file ast/method-call-exp.hh
 ** \brief Declaration of ast::MethodCallExp.
 */

#pragma once

#include <ast/call-exp.hh>
#include <ast/method-dec.hh>
#include <ast/var.hh>

namespace ast
{
  /** \class ast::MethodCallExp
   ** \brief Method call.
   **
   ** A method call is \em not a function call in the strict sense
   ** of object-oriented programming.  Inheritance is used as a
   ** factoring tool here.
   */

  class MethodCallExp : public CallExp
  {
    // DONE
  public:
    /** \name Ctor & dtor.
     ** \{ */
    /// Construct an MethodCallExp node.
    MethodCallExp(const Location& location,
           misc::symbol name,
           ast::exps_type* args,
           ast::Var* object);
    MethodCallExp(const MethodCallExp&) = delete;
    MethodCallExp& operator=(const MethodCallExp&) = delete;
    /// Destroy an MethodCallExp node.
    /** \} */

    /// \name Visitors entry point.
    /// \{ */
    /// Accept a const visitor \a v.
    void accept(ConstVisitor& v) const override;
    /// Accept a non-const visitor \a v.
    void accept(Visitor& v) override;
    /// \}

    /** \name Accessors.
     ** \{ */
    /// Return stored integer value.
    misc::symbol name_get() const;
    ast::exps_type* args_get() const;
    ast::Var* object_get() const;
    /** \} */

  protected:
    /// Stored integer value.
    misc::symbol name_;
    ast::exps_type* args_;
    ast::Var* object_;
  };
} // namespace ast
#include <ast/method-call-exp.hxx>
