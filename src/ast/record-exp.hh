/**
 ** \file ast/record-exp.hh
 ** \brief Declaration of ast::RecordExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/field-init.hh>
#include <ast/name-ty.hh>

namespace ast
{
  /// RecordExp.
  class RecordExp : public Exp
  {
  public:
    // DONE
    RecordExp(const Location& location,
              ast::NameTy* type_name,
              ast::fieldinits_type* fields);
    RecordExp(const RecordExp&) = delete;
    RecordExp& operator=(const RecordExp&) = delete;
    /// Destroy an RecordExp node.
    /** \} */

    /// \name Visitors entry point.
    /// \{ */
    /// Accept a const visitor \a v.
    void accept(ConstVisitor& v) const override;
    /// Accept a non-const visitor \a v.
    void accept(Visitor& v) override;
    /// \}

    /** \name Accessors.
     ** \{ */
    /// Return stored integer value.
    ast::NameTy* type_name_get() const;
    ast::fieldinits_type* fields_get() const;
    const TypeDec* def_get() const;
    TypeDec* def_get();
    void def_set(TypeDec*);
    /** \} */

  protected:
    /// Stored integer value.
    ast::NameTy* type_name_;
    ast::fieldinits_type* fields_;
    TypeDec* def_ = nullptr;
  };
} // namespace ast
#include <ast/record-exp.hxx>
