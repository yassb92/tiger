/**
 ** \file ast/record-ty.hxx
 ** \brief Inline methods of ast::RecordTy.
 */

#pragma once

#include <ast/record-ty.hh>

namespace ast
{
  // DONE
  inline ast::fields_type* RecordTy::fields_get() const { return fields_; }
  inline void RecordTy::fields_set(ast::fields_type* fields) { fields_ = fields; }

  inline const TypeDec* RecordTy::def_get() const { return def_; }
  inline TypeDec* RecordTy::def_get() { return def_; }
  inline void RecordTy::def_set(TypeDec* def) { def_ = def; }
} // namespace ast
