/**
 ** \file ast/pretty-printer.cc
 ** \brief Implementation of ast::PrettyPrinter.
 */

#include "pretty-printer.hh"
#include <ast/all.hh>
#include <ast/libast.hh>
#include <ast/pretty-printer.hh>
#include <misc/escape.hh>
#include <misc/indent.hh>
#include <misc/separator.hh>
#include "type-dec.hh"

#include <type/class.hh>

namespace ast
{
  // Anonymous namespace: these functions are private to this file.
  namespace
  {
    /// Output \a e on \a ostr.
    inline std::ostream& operator<<(std::ostream& ostr, const Escapable& e)
    {
      if (escapes_display(ostr)
          // FIXME: Some code was deleted here.
      )
        ostr << "/* escaping */ ";

      return ostr;
    }

    /// \brief Output \a e on \a ostr.
    ///
    /// Used to factor the output of the name declared,
    /// and its possible additional attributes.
    inline std::ostream& operator<<(std::ostream& ostr, const Dec& e)
    {
      ostr << e.name_get();
      if (bindings_display(ostr))
        ostr << " /* " << &e << " */";
      return ostr;
    }
  } // namespace

  PrettyPrinter::PrettyPrinter(std::ostream& ostr)
    : ostr_(ostr)
  {}

  void PrettyPrinter::operator()(const SimpleVar& e)
  {
    ostr_ << e.name_get();
    if (bindings_display(ostr_))
      ostr_ << " /* " << e.def_get() << " */";
  }

  void PrettyPrinter::operator()(const FieldVar& e)
  {
    // DONE
    ostr_ << *e.var_get() << ".";
    ostr_ << e.name_get();
  }

  /* Foo[10]. */
  void PrettyPrinter::operator()(const SubscriptVar& e)
  {
    ostr_ << e.var_get() << '[' << misc::incindent << e.index_get()
          << misc::decindent << ']';
  }

  void PrettyPrinter::operator()(const CastExp& e)
  {
    ostr_ << "_cast(" << e.exp_get() << ", " << e.ty_get() << ')';
  }

  // FIXME: Some code was deleted here.
  void PrettyPrinter::operator()(const ArrayExp& e)
  {
    ostr_ << *e.get_type() << " [" << *e.get_size() << "] of " << *e.get_init();
  }

  void PrettyPrinter::operator()(const AssignExp& e)
  {
    ostr_ << *e.var_get() << " := " << *e.exp_get();
  }

  void PrettyPrinter::operator()(const BreakExp& e)
  {
    ostr_ << "break";
    if (bindings_display(ostr_))
        ostr_ << " /* " << e.def_get() << " */";
  }
  void PrettyPrinter::operator()(const CallExp& e)
  {
    ostr_ << e.name_get();
    if (bindings_display(ostr_))
        ostr_ << " /* " << e.fundec_get() << " */";
    ostr_ << "(" << misc::separate(*e.args_get(), ", ") << ")";
  }

  void PrettyPrinter::operator()(const ForExp& e)
  {
    ostr_ << "for ";
    if (bindings_display(ostr_))
        ostr_ << " /* " << &e << " */ ";
    ostr_ << (const Dec&)(e.vardec_get())
          << " := " << *e.vardec_get().init_get() << " to " << e.hi_get()
          << " do" << misc::incendl << e.body_get() << misc::decindent;
  }

  void PrettyPrinter::operator()(const IfExp& e)
  {
    ostr_ << "if " << misc::incindent << *e.test_get() << misc::iendl << "then "
          << *e.thenclause_get() << misc::decindent;
    if (e.elseclause_get() != nullptr)
      {
        ostr_ << misc::incendl << "else " << *e.elseclause_get()
              << misc::decindent;
      }
  }

  void PrettyPrinter::operator()(const IntExp& e) { ostr_ << e.value_get(); }

  void PrettyPrinter::operator()(const LetExp& e)
  {
    //std::cout<<"je suis dans un LetEXp\n";
    ostr_ << "let" << misc::incendl
          << misc::separate(*e.decs_get(), misc::iendl) << misc::decendl << "in"
          << misc::incendl << *e.body_get() << misc::decendl << "end";
  }

  void PrettyPrinter::operator()(__attribute__((unused)) const NilExp& e)
  {
    ostr_ << "nil";
  }

  void PrettyPrinter::operator()(const OpExp& e)
  {
    switch (e.oper_get())
      {
      case ast::OpExp::Oper::add:
        ostr_ << e.left_get() << " + " << e.right_get();
        break;
      case ast::OpExp::Oper::sub:
        ostr_ << e.left_get() << " - " << e.right_get();
        break;
      case ast::OpExp::Oper::mul:
        ostr_ << e.left_get() << " * " << e.right_get();
        break;
      case ast::OpExp::Oper::div:
        ostr_ << e.left_get() << " / " << e.right_get();
        break;
      case ast::OpExp::Oper::eq:
        ostr_ << e.left_get() << " = " << e.right_get();
        break;
      case ast::OpExp::Oper::ne:
        ostr_ << e.left_get() << " <> " << e.right_get();
        break;
      case ast::OpExp::Oper::lt:
        ostr_ << e.left_get() << " < " << e.right_get();
        break;
      case ast::OpExp::Oper::le:
        ostr_ << e.left_get() << " <= " << e.right_get();
        break;
      case ast::OpExp::Oper::gt:
        ostr_ << e.left_get() << " > " << e.right_get();
        break;
      case ast::OpExp::Oper::ge:
        ostr_ << e.left_get() << " >= " << e.right_get();
        break;
      default:
        ostr_ << " -" << e.right_get();
      }
  }
  void PrettyPrinter::operator()(const RecordExp& e)
  {
    ostr_ << *e.type_name_get() << " { "
          << misc::separate(*e.fields_get(), ", ") << " }";
  }

  void PrettyPrinter::operator()(const SeqExp& e)
  {
    if ((*e.exps_get()).size() == 0)
      {
        ostr_ << "()";
        return;
      }
    auto& exps = *e.exps_get();
    if ((*e.exps_get()).size() == 1)
      {
        ostr_ << *exps[0];
        return;
      }
    ostr_ << "(" << misc::incendl;
    const auto size = exps.size();
    for (long unsigned i = 0; i < size; i++)
      {
        if (i != size - 1)
          ostr_ << *exps[i] << ";" << misc::iendl;
        else
          ostr_ << *exps[i] << misc::decendl;
      }
    ostr_ << ")";
  }

  void PrettyPrinter::operator()(const StringExp& e)
  {
    ostr_ << "\"" << e.string_get() << "\"";
  }

  void PrettyPrinter::operator()(const WhileExp& e)
  {
    ostr_ << "while ";
    if (bindings_display(ostr_))
        ostr_ << " /* " << &e << " */ ";
    ostr_ << e.test_get() << misc::iendl << "do " << misc::incendl
          << e.body_get();
  }

  void PrettyPrinter::operator()(const ArrayTy& e)
  {
    ostr_ << "array of " << e.base_type_get();
  }
  void PrettyPrinter::operator()(const TypeDec& e)
  {
    ostr_ << "type " << e.name_get();
    if (bindings_display(ostr_))
      ostr_ << " /* " << &e << " */";
    ostr_ << " = " << e.ty_get();
  }
  void PrettyPrinter::operator()(const NameTy& e)
  {
    ostr_ << e.name_get();
    if (bindings_display(ostr_))
      ostr_ << " /* " << e.def_get() << " */";
  }

  void PrettyPrinter::operator()(const RecordTy& e)
  {
    ostr_ << "{ " << misc::separate(*e.fields_get(), ", ") << " }";
  }

  void PrettyPrinter::operator()(const ChunkList& e)
  {
    for (auto v = e.chunks_get().begin(); v != e.chunks_get().end(); v++)
      {
        ostr_ << **v;
        ostr_ << misc::iendl;
      }
  }

  void PrettyPrinter::operator()(const Field& e)
  {
    ostr_ << e.name_get() << " : " << e.type_name_get();
  }

  void PrettyPrinter::operator()(const FieldInit& e)
  {
    ostr_ << e.name_get() << " = " << e.init_get();
  }

  void PrettyPrinter::operator()(const FunctionDec& e)
  {
    //std::cout<<"je suis dans un function dec\n";
    if (e.body_get() == nullptr)
      {
        ostr_ << "primitive " << e.name_get() << "("
              << misc::separate(e.formals_get(), ", ") << ")";
        if (e.result_get() != nullptr)
          {
            ostr_ << " : " << *e.result_get();
          }
        ostr_ << misc::iendl;
      }
    else
      {
        ostr_ << "function " << e.name_get();
        if (bindings_display(ostr_))
          ostr_ << " /* " << &e << " */";
        ostr_ << "("
              << misc::separate(e.formals_get(), ", ") << ")";
        if (bindings_display(ostr_))
          ostr_ << " /* " << &e << " */";
        if (e.result_get() != nullptr)
          {
            ostr_ << " : " << *e.result_get();
          }
        ostr_ << " = " << misc::incendl << *e.body_get();
      }
  }

  void PrettyPrinter::operator()(const VarDec& e)
  {
    if (e.init_get() == nullptr)
      {
        ostr_ << e.name_get();
        if (bindings_display(ostr_))
          ostr_ << " /* " << &e << " */";
        if (e.type_name_get() != nullptr)
          {
            ostr_ << " : " << *e.type_name_get();
          }
      }
    else
      {
        ostr_ << "var " << e.name_get();
        if (bindings_display(ostr_))
          ostr_ << " /* " << &e << " */";
        if (e.type_name_get() != nullptr)
          {
            ostr_ << " : " << *e.type_name_get();
          }
        ostr_ << " := " << *e.init_get();
      }
  }

  void PrettyPrinter::operator()(const VarChunk& e)
  {
    auto vec = e.decs_get();
    auto taille = vec.size();
    if (taille != 0)
      {
        for (long unsigned int i = 0; i < taille - 1; i++)
          {
            vec[i]->accept(*this);
            ostr_ << ", ";
          }
        vec[taille - 1]->accept(*this);
      }
  }
  void PrettyPrinter::operator()(const TypeChunk& e)
  {
    auto vec = e.decs_get();
    auto taille = vec.size();
    if (taille != 0)
      {
        for (long unsigned int i = 0; i < taille - 1; i++)
          {
            vec[i]->accept(*this);
            ostr_ << misc::iendl;
          }
        vec[taille - 1]->accept(*this);
      }
  }

  void PrettyPrinter::operator()(const FunctionChunk& e)
  {
    auto vec = e.decs_get();
    auto taille = vec.size();
    if (taille != 0)
      {
        for (long unsigned int i = 0; i < taille - 1; i++)
          {
            vec[i]->accept(*this);
            ostr_ << misc::iendl;
          }
        vec[taille - 1]->accept(*this);
      }
  }

} // namespace ast
// namespace ast
