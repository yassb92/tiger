/**
 ** \file ast/field-var.hh
 ** \brief Declaration of ast::FieldVar.
 */

#pragma once

#include <ast/var.hh>
#include <misc/symbol.hh>

namespace ast
{
  /// FieldVar.
  class FieldVar : public Var
  {
    // DONE
  public:
    /** \name Ctor & dtor.
     ** \{ */
    /// Construct a Field node.
    FieldVar(const Location& location, ast::Var* var, misc::symbol name);
    FieldVar(const FieldVar&) = delete;
    FieldVar& operator=(const FieldVar&) = delete;
    /// \name Visitors entry point.
    /// \{ */
    /// Accept a const visitor \a v.
    void accept(ConstVisitor& v) const override;
    /// Accept a non-const visitor \a v.
    void accept(Visitor& v) override;
    /// \}
    /** \name Accessors.
     ** \{ */
    /// Return stored integer value.
    ast::Var* var_get() const;
    misc::symbol name_get() const;
    /** \} */

  protected:
    ast::Var* var_;
    misc::symbol name_;
  };
} // namespace ast
#include <ast/field-var.hxx>
