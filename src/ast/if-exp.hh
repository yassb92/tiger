/**
 ** \file ast/if-exp.hh
 ** \brief Declaration of ast::IfExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/seq-exp.hh>

namespace ast
{
  /// IfExp.
  class IfExp : public Exp
  {
  public:
    // DONE
    IfExp(const Location& location, ast::Exp* test, ast::Exp* thenclause);

    IfExp(const Location& location,
          ast::Exp* test,
          ast::Exp* thenclause,
          ast::Exp* elseclause);
    IfExp(const IfExp&) = delete;
    IfExp& operator=(const IfExp&) = delete;
    /// Destroy an IfExp node.
    /** \} */

    /// \name Visitors entry point.
    /// \{ */
    /// Accept a const visitor \a v.
    void accept(ConstVisitor& v) const override;
    /// Accept a non-const visitor \a v.
    void accept(Visitor& v) override;
    /// \}

    /** \name Accessors.
     ** \{ */
    /// Return stored integer value.
    ast::Exp* test_get() const;
    ast::Exp* thenclause_get() const;
    ast::Exp* elseclause_get() const;
    /** \} */

  protected:
    /// Stored integer value.
    ast::Exp* test_;
    ast::Exp* thenclause_;
    ast::Exp* elseclause_;
  };
} // namespace ast
#include <ast/if-exp.hxx>
