/**
 ** \file ast/call-exp.hxx
 ** \brief Inline methods of ast::CallExp.
 */

#pragma once

#include <ast/call-exp.hh>
#include "function-dec.hh"

namespace ast
{
  // DONE
  inline misc::symbol CallExp::name_get() const { return name_; }
  inline ast::exps_type* CallExp::args_get() const { return args_; }
  inline FunctionDec* CallExp::fundec_get() const { return fundec_; }
  inline void CallExp::fundec_set(FunctionDec* fundec) { fundec_ = fundec; }
  inline void CallExp::name_set(misc::symbol name) {name_ = name;}
} // namespace ast
