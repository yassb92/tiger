/**
 ** \file ast/field-var.cc
 ** \brief Implementation of ast::FieldVar.
 */

#include <ast/field-var.hh>
#include <ast/visitor.hh>

namespace ast
{
  // DONE
  FieldVar::FieldVar(const Location& location, ast::Var* var, misc::symbol name)
    : Var(location)
    , name_(name)
    , var_(var)
  {}

  void FieldVar::accept(ConstVisitor& v) const { return v(*this); }

  void FieldVar::accept(Visitor& v) { return v(*this); }
} // namespace ast
