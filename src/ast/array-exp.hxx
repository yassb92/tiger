/**
 ** \file ast/array-exp.hxx
 ** \brief Inline methods of ast::ArrayExp.
 */

#pragma once

#include <ast/array-exp.hh>

namespace ast
{
  inline ast::Exp* ArrayExp::get_size() const { return size_; }
  inline ast::Exp* ArrayExp::get_init() const { return init_; }
  inline ast::NameTy* ArrayExp::get_type() const { return type_name_; }
  // DONE
} // namespace ast
