/**
 ** \file ast/array-exp.hh
 ** \brief Declaration of ast::ArrayExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/name-ty.hh>

namespace ast
{
  /// ArrayExp.
  class ArrayExp : public Exp
  {
    // DONE
  public:
    /** \name Ctor & dtor.
     ** \{ */
    /// Construct an ArrayExp node.
    ArrayExp(const Location& location,
             ast::NameTy* type_name,
             ast::Exp* size,
             ast::Exp* init);
    ArrayExp(const ArrayExp&) = delete;
    ArrayExp& operator=(const ArrayExp&) = delete;
    /// Destroy an ArrayExp node.
    /** \} */

    /// \name Visitors entry point.
    /// \{ */
    /// Accept a const visitor \a v.
    void accept(ConstVisitor& v) const override;
    /// Accept a non-const visitor \a v.
    void accept(Visitor& v) override;
    /// \}

    /** \name Accessors.
     ** \{ */
    /// Return stored integer value.
    ast::Exp* get_size() const;
    ast::Exp* get_init() const;
    ast::NameTy* get_type() const;
    /** \} */

  protected:
    /// Stored integer value.
    ast::NameTy* type_name_;
    ast::Exp* size_;
    ast::Exp* init_;
  };
} // namespace ast
#include <ast/array-exp.hxx>
