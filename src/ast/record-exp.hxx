/**
 ** \file ast/record-exp.hxx
 ** \brief Inline methods of ast::RecordExp.
 */

#pragma once

#include <ast/record-exp.hh>

namespace ast
{
  // DONE
  inline ast::NameTy* RecordExp::type_name_get() const { return type_name_; }
  inline ast::fieldinits_type* RecordExp::fields_get() const { return fields_; }
  inline const TypeDec* RecordExp::def_get() const { return def_; }
  inline TypeDec* RecordExp::def_get() { return def_; }
  inline void RecordExp::def_set(TypeDec* def) { def_ = def; }
} // namespace ast
