/**
 ** \file ast/call-exp.hh
 ** \brief Declaration of ast::CallExp.
 */

#pragma once

#include <ast/exp.hh>
#include <ast/function-dec.hh>
#include <misc/symbol.hh>
#include "function-dec.hh"

namespace ast
{
  /// CallExp.
  class CallExp : public Exp
  {
    // DONE
  public:
    CallExp(const Location& location, misc::symbol name, ast::exps_type* args);
    CallExp(const CallExp&) = delete;
    CallExp& operator=(const CallExp&) = delete;

    /// \name Visitors entry point.
    /// \{ */
    /// Accept a const visitor \a v.
    void accept(ConstVisitor& v) const override;
    /// Accept a non-const visitor \a v.
    void accept(Visitor& v) override;
    /// \}

    misc::symbol name_get() const;
    ast::exps_type* args_get() const;
    ast::FunctionDec* fundec_get() const;
    void fundec_set(FunctionDec*);
    void name_set(misc::symbol);
  protected:
    misc::symbol name_;
    ast::exps_type* args_;
    FunctionDec* fundec_ = nullptr;
  };
} // namespace ast
#include <ast/call-exp.hxx>
