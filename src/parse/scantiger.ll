                                                            /* -*- C++ -*- */
// %option defines the parameters with which the reflex will be launched
%option noyywrap
// To enable compatibility with bison
%option bison-complete
%option bison-cc-parser=parser
%option bison_cc_namespace=parse
%option bison-locations

%option lex=lex
// Add a param of function lex() generate in Lexer class
%option params="::parse::TigerParser& tp"
%option namespace=parse
// Name of the class generate by reflex
%option lexer=Lexer

%top{

#define YY_EXTERN_C extern "C" // For linkage rule

#include <cerrno>
#include <climits>
#include <regex>
#include <string>

#include <boost/lexical_cast.hpp>

#include <misc/contract.hh>
  // Using misc::escape is very useful to quote non printable characters.
  // For instance
  //
  //    std::cerr << misc::escape('\n') << '\n';
  //
  // reports about `\n' instead of an actual new-line character.
#include <misc/escape.hh>
#include <misc/symbol.hh>
#include <parse/parsetiger.hh>
#include <parse/tiger-parser.hh>

  // DONE (Define YY_USER_ACTION to update locations).
#define YY_USER_ACTION tp.location_.columns(size());

#define TOKEN(Type)                             \
  parser::make_ ## Type(tp.location_)

#define TOKEN_VAL(Type, Value)                  \
  parser::make_ ## Type(Value, tp.location_)

# define CHECK_EXTENSION()                              \
  do {                                                  \
    if (!tp.enable_extensions_p_)                       \
      tp.error_ << misc::error::error_type::scan        \
                << tp.location_                         \
                << ": invalid identifier: `"            \
                << misc::escape(text()) << "'\n";       \
  } while (false)


%}

%x SC_COMMENT SC_STRING

/* Abbreviations.  */
int             [0-9]+
iendl           [\n]|[\r]|[\n\r]|[\r\n]
/* TODO USE SUBLEXERS */
id    [a-zA-Z]([a-zA-Z0-9_])*|_main

%class{
  // DONE (Local variables).
int level;
std::string grown_string;
}

%%
/* The rules.  */
/* Commentaires */
"/*" {level = 1; start(SC_COMMENT);}



/* White characters*/
[ \t] {continue;}
{iendl} {tp.location_.lines(1);}
{int}         {
                int val = 0;
  // DONE (Decode, and check the value).
                try 
                {
                  val += std::stoi(text());
                }
                catch (std::out_of_range const& e)
                {
                  tp.error_ << misc::error::error_type::scan        \
                   << tp.location_                         \
                   << ": invalid identifier: `"            \
                   << misc::escape(yytext) << "'\n";
                }
                return TOKEN_VAL(INT, val);
              }

/* STRING */
"\"" {grown_string.clear(); start(SC_STRING);}


  /* DONE */
"&" {return TOKEN(AND);}
"array" {return TOKEN(ARRAY);}
":=" {return TOKEN(ASSIGN);}
"break" {return TOKEN(BREAK);}
"_cast" {return TOKEN(CAST);}
"class" {return TOKEN(CLASS);}
":" {return TOKEN(COLON);}
"," {return TOKEN(COMMA);}
"/" {return TOKEN(DIVIDE);}
"<>" {return TOKEN(NE);}
">=" {return TOKEN(GE);}
"<=" {return TOKEN(LE);}
"do" {return TOKEN(DO);}
"." {return TOKEN(DOT);}
"else" {return TOKEN(ELSE);}
"end" {return TOKEN(END);}
"=" {return TOKEN(EQ);}
"extends" {return TOKEN(EXTENDS);}
"for" {return TOKEN(FOR);}
"function" {return TOKEN(FUNCTION);}
">" {return TOKEN(GT);}
"if" {return TOKEN(IF);}
"import" {return TOKEN(IMPORT);}
"in" {return TOKEN(IN);}
"{" {return TOKEN(LBRACE);}
"[" {return TOKEN(LBRACK);}
"let" {return TOKEN(LET);}
"(" {return TOKEN(LPAREN);}
"<" {return TOKEN(LT);}
"-" {return TOKEN(MINUS);}
"method" {return TOKEN(METHOD);}
"new" {return TOKEN(NEW);}
"nil" {return TOKEN(NIL);}
"of" {return TOKEN(OF);}
"|" {return TOKEN(OR);}
"+" {return TOKEN(PLUS);}
"primitive" {return TOKEN(PRIMITIVE);}
"}" {return TOKEN(RBRACE);}
"]" {return TOKEN(RBRACK);}
")" {return TOKEN(RPAREN);}
";" {return TOKEN(SEMI);}
"then" {return TOKEN(THEN);}
"*" {return TOKEN(TIMES);}
"to" {return TOKEN(TO);}
"type" {return TOKEN(TYPE);}
"var" {return TOKEN(VAR);}
"while" {return TOKEN(WHILE);}
"_chunks" {return TOKEN(CHUNKS);}
"_exp" {return TOKEN(EXP);}
"_lvalue" {return TOKEN(LVALUE);}
"_namety" {return TOKEN(NAMETY);}
/*<<EOF>> {return TOKEN(EOF);}*/

/*"\r" {tp.location_.lines(0);}
"\r\n" {tp.location_.lines(1);}
"\n" {tp.location_.lines(1); std::cout << "ok";}
"\n\r" {tp.location_.lines(1);}*/
{id}  {
                  return TOKEN_VAL(ID, text());
      }
. {tp.error_ << misc::error::error_type::scan        \
                   << tp.location_                         \
                   << ": unknown symbol " << text()            \
                   << "\n";}

<SC_COMMENT>  {
{iendl} {tp.location_.lines(1);}
"/*" {++level;}
"*/" {if (--level == 0) start(INITIAL);}
<<EOF>> {tp.error_ << misc::error::error_type::scan        \
                   << tp.location_                         \
                   << ": unexpected end of file in a comment"            \
                   << "\n"; start(INITIAL);}
. ;
}
<SC_STRING> {

"\""  {
                      start(INITIAL);
                      return TOKEN_VAL(STRING, grown_string);
                    }
\\a|\\b|\\f|\\n|\\r|\\t|\\v {
                grown_string += text();
              }
\\[0-7]{3} {
                grown_string += strtol(text() + 1, 0, 8);
              }
\\x[0-9a-fA-f]{2} {
                grown_string += strtol(text() + 2, 0, 16);
              }
\\\\ {
                grown_string += text();
              }
\\\" {
                grown_string += text();
              }
\\. {
                tp.error_ << misc::error::error_type::scan        \
                   << tp.location_                         \
                   << ": invalid identifier: `"            \
                   << misc::escape(yytext) << "'\n";
              }
<<EOF>> {tp.error_ << misc::error::error_type::scan        \
                   << tp.location_                         \
                   << ": unexpected end of file in a string"            \
                   << misc::escape(yytext) << "\n"; start(INITIAL);}
. {
                grown_string += text();
              }
}
%%