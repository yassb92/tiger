/**
 ** \file bind/renamer.cc
 ** \brief Implementation of bind::Renamer.
 */

#include <bind/renamer.hh>

namespace bind
{
  using namespace ast;

  Renamer::Renamer()
    : super_type()
    , new_names_()
  {}

  // DONE

  void Renamer::operator()(ast::VarDec& e)
  {
    // + recursively rename si y a des nested expressions
    visit<>(e, &e);
  }

  void Renamer::operator()(ast::TypeDec& e)
  {
    // + recursively rename si y a des nested expressions
    visit<>(e, &e);
  }

  void Renamer::operator()(ast::FunctionDec& e)
  {
    if (!e.body_get())
      {
        super_type::operator()(e);
      }
    else
      {
        // + recursively rename si y a des nested expressions
        visit<>(e, &e);
      }
  }


  //EXACTEMENT la meme fct que VarDec pr FunctionDec et TypeDec
  void Renamer::operator()(ast::SimpleVar& e) { visit<>(e, e.def_get()); }
  void Renamer::operator()(ast::NameTy& e) { visit<>(e, e.def_get()); }
  void Renamer::operator()(ast::CallExp& e) { visit<>(e, e.fundec_get()); }
  void Renamer::operator()(ast::RecordExp& e)
  {
    visit<>(*e.type_name_get(), e.def_get());
  }

} // namespace bind
