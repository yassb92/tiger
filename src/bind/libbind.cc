/**
 ** \file bind/libbind.cc
 ** \brief Define exported bind functions.
 */

// DONE
#include <bind/binder.hh>
#include <bind/libbind.hh>
#include <bind/renamer.hh>

namespace bind
{
  void bindings_compute(ast::Ast& tree)
  {
    Binder bind_compute;
    bind_compute(tree);
    bind_compute.error_get().exit_on_error();
  }

  void renamer_compute(ast::Ast& tree)
  {
    Renamer rename_compute;
    rename_compute(tree);
  }

} // namespace bind

