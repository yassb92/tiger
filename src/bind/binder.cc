/**
 ** \file bind/binder.cc
 ** \brief Implementation for bind/binder.hh.
 */

#include <ast/all.hh>
#include <bind/binder.hh>

#include <misc/contract.hh>

namespace bind
{
  /*-----------------.
  | Error handling.  |
  `-----------------*/

  /// The error handler.
  const misc::error& Binder::error_get() const { return error_; }

  // DONE(Error reporting).
  template <class T> void Binder::undeclared(const std::string& k, const T& e)
  {
    std::string err = k + e.name_get().get() + "\n";
    error(e, err);
  }

  template <class T> void Binder::unknown(const std::string& k, const T& e)
  {
    std::string err = k + e.type_name_get()->name_get().get() + "\n";
    error(e, err);
  }

  template <class T> void Binder::redefinition(const T& e1, const T& e2)
  {
    std::string redef = "redefinition: " + e1.name_get().get() + "\n";
    std::string firstdef = "first definition\n";
    error(e1, redef);
    error(e2, firstdef);
  }

  int Binder::get_count_main() const { return count_main_; }
  void Binder::check_main(const ast::FunctionDec& e)
  {
    // DONE
    if (e.name_get() == "_main" && get_count_main() != 0)
      redefinition(*scope_fun_.get("_main"), e);
    else if (e.name_get() == "_main" && get_count_main() == 0)
      {
        count_main_++;
      }
    else
      return;
  }

  /*----------------.
  | Symbol tables.  |
  `----------------*/

  void Binder::scope_begin()
  {
    // DONE
    this->scope_var_.scope_begin();
    this->scope_fun_.scope_begin();
    this->scope_type_.scope_begin();
  }

  void Binder::scope_end()
  {
    // DONE:
    this->scope_var_.scope_end();
    this->scope_fun_.scope_end();
    this->scope_type_.scope_end();
  }

  /*---------.
  | Visits.  |
  `---------*/

  // FIXME: Some code was deleted here.

  void Binder::operator()(ast::LetExp& e)
  {
    // DONE
    scope_begin();
    if (e.decs_get())
      e.decs_get()->accept(*this);
    if (e.body_get())
      e.body_get()->accept(*this);
    scope_end();
  }

  /*inline bool is_primitive(misc::symbol name)
  {
    if (name != "print" && name != "print_err" && name != "print_int"
        && name != "flush" && name != "getchar" && name != "ord"
        && name != "size" && name != "streq" && name != "strcmp"
        && name != "substring" && name != "concat" && name != "not"
        && name != "exit")
      return false;
    return true;
  }*/

  void Binder::operator()(ast::CallExp& e)
  {
    for (const auto& arg : *e.args_get())
      {
        if (arg)
          {
            arg->accept(*this);
          }
      }
    ast::FunctionDec* fundec = scope_fun_.get(e.name_get());
    if (fundec == nullptr)
      {
        undeclared("Undeclared function: ", e);
      }
    else
      {
        e.fundec_set(fundec);
      }
  }
  void Binder::operator()(ast::Ast& e)
  {
    scope_begin();
    super_type::operator()(e);
    scope_end();
  }
  void Binder::operator()(ast::NameTy& e)
  {
    ast::TypeDec* type = scope_type_.get(e.name_get());
    if (type == nullptr)
      {
        if (e.name_get() != "string" && e.name_get() != "int")
          undeclared("Undeclared type: ", e);
      }
    else
      {
        e.def_set(type);
      }
  }
  void Binder::operator()(ast::SimpleVar& e)
  {
    ast::VarDec* var = scope_var_.get(e.name_get());
    if (var == nullptr)
      {
        undeclared("Undeclared variable: ", e);
      }
    else
      {
        e.def_set(var);
      }
  }

  void Binder::operator()(ast::WhileExp& e)
  {
    auto save = loop_;
    loop_ = &e;
    scope_begin();
    e.test_get().accept(*this);
    e.body_get().accept(*this);
    scope_end();
    loop_ = save;
  }
  void Binder::operator()(ast::ForExp& e)
  {
    auto save = loop_;
    loop_ = &e;
    scope_begin();
    scope_var_.put(e.vardec_get().name_get(), &e.vardec_get());
    e.vardec_get().accept(*this);
    e.hi_get().accept(*this);
    e.body_get().accept(*this);
    scope_end();
    loop_ = save;
  }

  void Binder::operator()(ast::RecordExp& e)
  {
    ast::TypeDec* type = scope_type_.get(e.type_name_get()->name_get());
    if (type == nullptr)
      {
        unknown("Unknown type: ", e);
      }
    else
      {
        e.def_set(type);
      }
  }

  void Binder::operator()(ast::BreakExp& e)
  {
    if (loop_ == nullptr)
      {
        error(e, "break outside loop\n");
      }
    else
      {
        e.def_set(loop_);
      }
  }

  /*-------------------.
  | Visiting VarChunk. |
  `-------------------*/

  // DONE
  void Binder::operator()(ast::VarChunk& e) { chunk_visit<>(e); }

  /*------------------------.
  | Visiting FunctionChunk. |
  `------------------------*/
  // DONE
  void Binder::operator()(ast::TypeChunk& e) { chunk_visit<>(e); }

  /*--------------------.
  | Visiting TypeChunk. |
  `--------------------*/
  // DONE
  void Binder::operator()(ast::FunctionChunk& e) { chunk_visit<>(e); }

} // namespace bind
