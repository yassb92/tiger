/**
 ** \file bind/tasks.hh
 ** \brief Bind module related tasks.
 */

// FIXME: Some code was deleted here.

#pragma once

#include <task/libtask.hh>

namespace bind::tasks
{
    TASK_GROUP("Bindings");

    /// Compute variables escaping.
    TASK_DECLARE("b|bindings-compute",
                "compute the bindings variables and the functions requiring a static link",
                bindings_compute,
                "parse");
    /// Display binding variables.
    TASK_DECLARE("B|bindings-display",
                "enable bindings display in the AST",
                bindings_display,
                "parse");

    /// Renamer
    TASK_DECLARE("rename",
                "renamer enable",
                renamer,
                "bound");
    
    DISJUNCTIVE_TASK_DECLARE("bound", "", "bindings-compute");// object-bindings-compute combine-types-compute");
}
