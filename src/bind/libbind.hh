/**
 ** \file bind/libbind.hh
 ** \brief Interface of the bind module.
 */

// DONE
#pragma once

#include <ast/fwd.hh>
#include <misc/error.hh>

/// Computing escape and static link related information.
namespace bind
{
  /// Compute the escaping variables.
  void bindings_compute(ast::Ast& tree);

  void renamer_compute(ast::Ast& tree);

} // namespace bind
