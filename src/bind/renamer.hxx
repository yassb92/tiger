/**
 ** \file bind/renamer.hxx
 ** \brief Template methods of bind::Renamer.
 */

#pragma once

#include <bind/renamer.hh>
#include <lib/misc/symbol.hh>

namespace bind
{

  // DONE

  //methode qui traverse l ast et applique le renommage process recursivement ds les nested expressions
  template <class E, class Def> void Renamer::visit(E& e, const Def* def)
  {
    // DONE
    if (def)
      {
        e.name_set(new_name(def));
      }
    super_type::operator()(e);
  }

  template <typename Def> misc::symbol Renamer::new_name_compute(const Def& e)
  {
    //si _main ou prim on retourne le nom original
    if (e->name_get().get() == "_main")
      {
        return e->name_get().get();
      }
    return misc::symbol::fresh(e->name_get());
  }

  template <typename Def> misc::symbol Renamer::new_name(const Def& e)
  {
    auto p = new_names_.find(e);
    //std::cout << "Looking for " << e->name_get() << std::endl;
    if (p != new_names_.end())
      {
        //std::cout<< "Found " << p->second << std::endl;
        return p->second;
      }
    else
      {
        misc::symbol name = new_name_compute(e);
        new_names_.insert({e, name});
        //std::cout << "Renaming " << e->name_get() << " to " << name << std::endl;
        return name;
      }
  }

} // namespace bind
