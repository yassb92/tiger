/**
 ** \file bind/binder.hxx
 ** \brief Inline methods of bind::Binder.
 **/

#pragma once

#include <bind/binder.hh>

namespace bind
{
  /*-----------------.
  | Error handling.  |
  `-----------------*/

  // DONE: Some code was deleted here (Error reporting).
  inline void Binder::error(const ast::Ast& loc, const std::string& msg)
  {
    error_ << misc::error::error_type::bind
           << loc.location_get()
           << ": "
           << msg;
  }
  /*----------------------------.
  | Visiting /ChunkInterface/.  |
  `----------------------------*/

  template <class D> void Binder::chunk_visit(ast::Chunk<D>& e)
  {
    // Shorthand.
    using chunk_type = ast::Chunk<D>;
    // DONE (Two passes: once on headers, then on bodies).
    //chunk_type elm = e.decs_get();
    for (const auto v : e)
      {
        visit_dec_header<D>(*v);
      }
    for (const auto v : e)
      {
        visit_dec_body<D>(*v);
      }
  }

  /* These specializations are in bind/binder.hxx, so that derived
     visitors can use them (otherwise, they wouldn't see them).  */

  // Insert the prototype of the function in the environment.
  template <>
  inline void Binder::visit_dec_header<ast::FunctionDec>(ast::FunctionDec& e)
  {
    // DONE: Some code was deleted here.
    ast::FunctionDec* header = scope_fun_.get(e.name_get());
    if (header == nullptr)
      {
        this->scope_fun_.put(e.name_get(), &e);
      }
    else
      {
        redefinition(*header, e);
      }
  }

  template <> inline void Binder::visit_dec_header<ast::VarDec>(ast::VarDec& e)
  {
    // DONE: Some code was deleted here.
    this->scope_var_.put(e.name_get(), &e);
  }
  template <>
  inline void Binder::visit_dec_header<ast::TypeDec>(ast::TypeDec& e)
  {
    // DONE: Some code was deleted here.
    ast::TypeDec* header = scope_type_.get(e.name_get());
    if (header == nullptr)
      {
        this->scope_type_.put(e.name_get(), &e);
      }
    else
      {
        if (e.name_get() != "string" && e.name_get() != "int")
          redefinition(*header, e);
      }
  }
  // Compute the bindings of this function's body.
  template <>
  inline void Binder::visit_dec_body<ast::FunctionDec>(ast::FunctionDec& e)
  {
    scope_begin();
    // DONE: Some code was deleted here.
    e.formals_get().accept(*this);
    std::map<misc::symbol, ast::VarDec*> v;
    for (const auto &f : e.formals_get())
      {
        if (v.find(f->name_get()) == v.end())
          v.insert(std::pair<misc::symbol, ast::VarDec*>(f->name_get(), f));
        else
          redefinition(*(v.find(f->name_get())->second), *f);
      }
    if (e.result_get())
    {
      e.result_get()->accept(*this);
    }
    if (e.body_get())
      e.body_get()->accept(*this);
    scope_end();
  }

  template <> inline void Binder::visit_dec_body<ast::VarDec>(ast::VarDec& e)
  {
    if (e.type_name_get())
      e.type_name_get()->accept(*this);
    if (e.init_get())
      e.init_get()->accept(*this);
  }

  template <> inline void Binder::visit_dec_body<ast::TypeDec>(ast::TypeDec& e)
  {
    e.ty_get().accept(*this);
  } 
} // namespace bind